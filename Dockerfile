FROM mcr.microsoft.com/dotnet/aspnet:6.0 as deploy-env
RUN apt update && apt install -y \
                    rsyslog
RUN service rsyslog start


FROM mcr.microsoft.com/dotnet/sdk:6.0 as build-env
WORKDIR /source
COPY *.sln .
COPY JapAPI/*.csproj ./JapAPI/
RUN dotnet restore
COPY . .
RUN dotnet publish -c release -o /app


FROM deploy-env
WORKDIR /app
COPY --from=build-env /app .
ENTRYPOINT ["./JapAPI"]