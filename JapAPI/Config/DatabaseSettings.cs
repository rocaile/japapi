﻿namespace JapAPI;

public class DatabaseSettings
{
    public string DatabaseName { get; set; } = null!;

    public string LessonsCollectionName { get; set; } = null!;
}
