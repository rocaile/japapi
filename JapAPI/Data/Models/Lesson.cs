using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace JapAPI;

public class Lesson
{
    public Lesson(Translation[] translations, string? id = null)
    {
        Id = id;
        Translations = translations;
    }

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }
    
    public Translation[] Translations { get; set; }
}