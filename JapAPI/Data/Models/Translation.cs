namespace JapAPI;

public class Translation
{
    public Translation(Word japanese, Word french)
    {
        Japanese = japanese;
        French = french;
    }

    public Word Japanese { get; set; }

    public Word French { get; set; }
}