namespace JapAPI;

public class Word
{
    public Word(string name, string? details = null)
    {
        Name = name;
        Details = details;
    }

    public string Name { get; set; }
    
    public string? Details { get; set; }
}