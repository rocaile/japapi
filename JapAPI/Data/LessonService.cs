﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace JapAPI.Data;

public class LessonService
{
    private readonly IMongoCollection<Lesson> _lessonsCollection;

    public LessonService(
        IOptions<DatabaseSettings> databaseSettings)
    {
        var mongoClient = new MongoClient(
            Environment.GetEnvironmentVariable("ConnectionString"));

        var mongoDatabase = mongoClient.GetDatabase(
            databaseSettings.Value.DatabaseName);

        _lessonsCollection = mongoDatabase.GetCollection<Lesson>(
            databaseSettings.Value.LessonsCollectionName);
    }

    public async Task<List<Lesson>> GetAsync() =>
        await _lessonsCollection.Find(_ => true).ToListAsync();

    public async Task<Lesson?> GetAsync(string id) =>
        await _lessonsCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task CreateAsync(Lesson newLesson) =>
        await _lessonsCollection.InsertOneAsync(newLesson);

    public async Task UpdateAsync(string id, Lesson updatedLesson) =>
        await _lessonsCollection.ReplaceOneAsync(x => x.Id == id, updatedLesson);

    public async Task RemoveAsync(string id) =>
        await _lessonsCollection.DeleteOneAsync(x => x.Id == id);
}