using JapAPI.Data;
using Microsoft.AspNetCore.Mvc;

namespace JapAPI.Controllers;

[ApiController]
[Route("api/[controller]")]
public class LessonController : ControllerBase
{
    private readonly ILogger<LessonController> _logger;
    private readonly LessonService _lessonService;

    public LessonController(ILogger<LessonController> logger,
        LessonService lessonService)
    {
        _logger = logger;
        _lessonService = lessonService;
    }

    [HttpGet]
    public async Task<IEnumerable<Lesson>> Get()
    {
        return await _lessonService.GetAsync();
    }

    [HttpGet("{id:length(24)}")]
    public async Task<ActionResult<Lesson>> Get(string id)
    {
        var lesson = await _lessonService.GetAsync(id);

        if (lesson is null)
        {
            return NotFound();
        }

        return lesson;
    }

    [HttpPost]
    public async Task<IActionResult> Post(Lesson newLesson)
    {
        await _lessonService.CreateAsync(newLesson);

        return CreatedAtAction(nameof(Get), new { id = newLesson.Id }, newLesson);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, Lesson updatedLesson)
    {
        var lesson = await _lessonService.GetAsync(id);

        if (lesson is null)
        {
            return NotFound();
        }

        updatedLesson.Id = lesson.Id;

        await _lessonService.UpdateAsync(id, updatedLesson);

        return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        var lesson = await _lessonService.GetAsync(id);

        if (lesson is null)
        {
            return NotFound();
        }

        await _lessonService.RemoveAsync(id);

        return NoContent();
    }
}